#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
Created on Sun Jun 2 12:18:35 2019
-----
"""

import urllib3
import json
import os

def get_api_data(api_data,key,*args):
    api_data = api_data[key]
    try:
        for arg in args:
            subset_data = api_data[arg]
        return subset_data
    except:
        return api_data

def fetch_api(api_url):
    http = urllib3.PoolManager()
    data = http.request('GET', api_url)
    data = json.loads(data.data.decode('UTF-8'))
    return data

def main(api_v, project):
    base_api = f"https://papermc.io/api/{api_v}/{project}"
    # Fetch project info
    project_data = fetch_api(base_api)
    project_version = get_api_data(project_data,"versions")[0]
    # Fetch project versions
    project_url = f"{base_api}/{project_version}"
    version_data = fetch_api(project_url)
    build_id = get_api_data(version_data,"builds","latest")
    # Return download url
    download_url = f"{project_url}/{build_id}/download"
    return download_url

if __name__ == "__main__":
    API_VERSION = "v1" #PAPER API version, currently there's no way to auto-fetch it
    PROJECT_NAME = "paper"
    DOWNLOAD_URL = main(API_VERSION,PROJECT_NAME)
    os.system(f"wget {DOWNLOAD_URL} -O paperclip.jar")